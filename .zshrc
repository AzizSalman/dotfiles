export PATH="$PATH:$HOME/.local/bin:/usr/local/bin"
export ZSH="$HOME/.oh-my-zsh"
ZSH_THEME="arrow"
zstyle ':omz:update' mode auto 
zstyle ':omz:update' frequency 13
plugins=(git)
source $ZSH/oh-my-zsh.sh
export MANPATH="/usr/local/man:$MANPATH"
export LANG=en_US.UTF-8

if [[ -n $SSH_CONNECTION ]]; then
	export EDITOR='vi'
else
	export EDITOR='vim'
fi

# Compilation flags
export ARCHFLAGS="-arch x86_64"

alias zshconfig="vim ~/.zshrc"
alias sshconfig="vim ~/.ssh/config"
alias vimconfig="vim ~/.vimrc"
alias tmx="tmux"
alias sshot="ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null" 
alias gogit="git add . ; git commit "

unset SSH_AGENT_PID
if [ "${gnupg_SSH_AUTH_SOCK_by:-0}" -ne $$ ]; then
	  export SSH_AUTH_SOCK="$(gpgconf --list-dirs agent-ssh-socket)"
fi 
