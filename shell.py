#!/usr/bin/python3

# usage shell.py <distro>
# example shell.py ubuntu
# currently supported distro: ArchLinux DNF-based Ubuntu
import os
import subprocess
import sys

def run_command(command):
    process = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE)
    process.wait()

# Define function to install packages on Ubuntu
def ubuntu_install(packages):
    run_command(f'sudo apt update && sudo apt install -y {" ".join(packages)}')

# Define function to install packages on Arch Linux
def arch_install(packages):
    run_command(f'sudo pacman -Syyu --noconfirm && sudo pacman -S --noconfirm {" ".join(packages)}')

# Define function to install packages on AlmaLinux or Redhat
def dnf_install(packages):
    run_command(f'sudo dnf update && sudo dnf install -y {" ".join(packages)}')

# Define function to copy configuration files
def copy_configs():
    run_command('wget https://gitlab.com/AzizSalman/dotfiles/-/raw/main/.zshrc -O ~/.zshrc')
    run_command('wget https://gitlab.com/AzizSalman/dotfiles/-/raw/main/.vimrc -O ~/.vimrc')
    run_command('wget https://gitlab.com/AzizSalman/dotfiles/-/raw/main/.tmux.conf -O ~/.tmux.conf')
    run_command('git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim')

# Define function to install Oh My Zsh
def install_ohmyzsh():
    run_command('chsh -s /usr/bin/zsh')
    run_command('sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"')

# Define python
def install_pythonpip():
    run_command('python -m pip completion --zsh >> ~/.zshrc')
    run_command('python -m pip install --user virtualenv')

# Packages to install
packages = ['zsh', 'git', 'vim', 'curl', 'tmux', 'python3', 'python3-pip']

# Distro type from command line argument
distro_type = sys.argv[1]

# Check if distro is Ubuntu
if distro_type.lower() == 'ubuntu':
    ubuntu_install(packages)
# Check if distro is Arch Linux
elif distro_type.lower() == 'arch':
    arch_install(packages)
# Check if distro is AlmaLinux or Redhat
elif distro_type.lower() in ['almalinux', 'redhat']:
    dnf_install(packages)
# If distro is not supported, exit with an error
else:
    print('Unsupported distro')
    exit(1)

# Copy configuration files
copy_configs()
# Install Oh My Zsh
install_ohmyzsh()
install_pythonpip()