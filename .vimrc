set nocompatible
filetype on
syntax on
set number

set rtp+=~/.vim/bundle/Vundle.vim

call vundle#begin()
Plugin 'VundleVim/Vundle.vim'
" Plugin 'vimwiki/vimwiki'
" Plugin 'shime/vim-livedown'
" Plugin 'davidhalter/jedi-vim'
Plugin 'Valloric/YouCompleteMe'
Plugin 'scrooloose/nerdtree'
Plugin 'octol/vim-cpp-enhanced-highlight'
" Plugin 'chrisbra/csv.vim'
" Plugin 'ervandew/supertab'
" Plugin 'PotatoesMaster/i3-vim-syntax'
" Plugin 'farseer90718/vim-taskwarrior'
" Plugin 'tbabej/taskwiki'
" Plugin 'mattn/calendar-vim'
" Plugin 'ferrine/md-img-paste.vim'
call vundle#end() 

filetype plugin indent on 
" set the leader character
let mapleader=","

" settings for TreeToggle
map <leader>ee :NERDTreeToggle<cr>
map <leader>qq :q!<cr>
map <leader>xx :x<cr>
map <leader>eb :NERDTreeFromBookmark
map <leader>ef :NERDTreeFind<cr>

" Spell Checking
map <leader>o :setlocal spell! spelllang=en_us<CR>

" Split Settings
set splitbelow splitright
" Moving to another window keys
map <C-h> <C-w>h
map <C-j> <C-w>j
map <C-k> <C-w>k
map <C-l> <C-w>l


" Check file in shellcheck
map <leader>s :!clear && shellcheck %<CR>

" Load VimWiki to ther right
map <leader>b :vsp<space>~/wiki/index.wiki<CR>

" Disable auto commenting
autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o

" i copied these from https://blog.mague.com/?p=602 i dont know that much of it tho
au BufRead,BufNewFile *.wiki set filetype=vimwiki
au FileType vimwiki setlocal shiftwidth=6 tabstop=6 noexpandtab 


:autocmd FileType vimwiki map <leader>d :VimwikiMakeDiaryNote<CR>
function! ToggleCalendar()
  execute ":Calendar"
  if exists("g:calendar_open")
    if g:calendar_open == 1
      execute "q"
      unlet g:calendar_open
    else
      g:calendar_open = 1
    end
  else
    let g:calendar_open = 1
  end
endfunction
:autocmd FileType vimwiki map <leader>c :call ToggleCalendar()<CR> 

autocmd FileType markdown nmap <buffer><silent> <leader>p :call mdip#MarkdownClipboardImage()<CR>
" there are some defaults for image directory and image name, you can change them

" General shourtcut
" inoremap ;j <Esc>/<++><Enter>"_c4l

" cpp Highlighting
let g:cpp_class_scope_highlight = 1
let g:cpp_member_variable_highlight = 1
let g:cpp_class_decl_highlight = 1

if has("autocmd")
  augroup templates
    autocmd BufNewFile main.cpp 0r ~/.vim/templates/main.cpp
    autocmd BufNewFile *.sh 0r ~/.vim/templates/skeleton.sh
  augroup END
endif

" autocmd StdinReadPre * let s:std_in=1
" autocmd VimEnter * if argc() == 0 && !exists("s:std_in") | NERDTree | endif

" Let clangd fully control code completion
let g:ycm_clangd_uses_ycmd_caching = 0
let g:ycm_global_ycm_extra_conf = "~/.vim/.ycm_extra_conf.py"
let g:ycm_clangd_binary_path = exepath("clangd")


let g:SuperTabClosePreviewOnPopupClose = 1
let g:ycm_autoclose_preview_window_after_insertion = 1
let g:ycm_autoclose_preview_window_after_completion = 1
autocmd FileType ruby,eruby let g:rubycomplete_buffer_loading = 1 
autocmd FileType ruby,eruby let g:rubycomplete_classes_in_global = 1
autocmd FileType ruby,eruby let g:rubycomplete_rails = 1

autocmd FileType python set shiftwidth=4
autocmd FileType python set tabstop=4
autocmd FileType python set softtabstop=4

set arabicshape
colorscheme  ron 
